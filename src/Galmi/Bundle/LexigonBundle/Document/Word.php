<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 29.08.14
 * Time: 9:53
 */

namespace Galmi\Bundle\LexigonBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

/**
 * @MongoDB\Document(repositoryClass="\Galmi\Bundle\LexigonBundle\Document\WordRepository")
 * @MongoDB\Index(keys={"wordLength"="desc", "score"="desc"})
 */
class Word
{

    const DICT_ENGLISH = 'en';
    const DICT_RUSSIAN = 'ru';

    /**
     * @MongoDB\Id(strategy="auto")
     */
    private $id;

    /**
     * Найденное слово
     *
     * @MongoDB\String
     */
    private $word;

    /**
     * Длина слова
     *
     * @MongoDB\Int
     * @MongoDB\Index(unique=false, order="desc")
     */
    private $wordLength;

    /**
     * Кол-во заработанных очков за слово
     *
     * @MongoDB\Int
     * @MongoDB\Index(unique=false, order="desc")
     */
    private $score;

    /**
     * Кол-во найденных слов
     *
     * @MongoDB\Int
     */
    private $count;

    /**
     * @MongoDB\ReferenceOne(targetDocument="\Galmi\Bundle\LexigonBundle\Document\User")
     * @MongoDB\Index(unique=false)
     */
    private $user;

    /**
     * Тип соц сети
     *
     * @MongoDB\String
     * @MongoDB\Index(unique=false)
     */
    private $socialType;

    /**
     * Язык словаря слова
     *
     * @MongoDB\String
     */
    private $dict;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set word
     *
     * @param string $word
     * @return self
     */
    public function setWord($word)
    {
        $this->word = $word;
        return $this;
    }

    /**
     * Get word
     *
     * @return string $word
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * Set wordLength
     *
     * @param int $wordLength
     * @return self
     */
    public function setWordLength($wordLength)
    {
        $this->wordLength = $wordLength;
        return $this;
    }

    /**
     * Get wordLength
     *
     * @return int $wordLength
     */
    public function getWordLength()
    {
        return $this->wordLength;
    }

    /**
     * Set score
     *
     * @param int $score
     * @return self
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }

    /**
     * Get score
     *
     * @return int $score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set count
     *
     * @param int $count
     * @return self
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * Get count
     *
     * @return int $count
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set user
     *
     * @param \Galmi\Bundle\LexigonBundle\Document\User $user
     * @return self
     */
    public function setUser(\Galmi\Bundle\LexigonBundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \Galmi\Bundle\LexigonBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set socialType
     *
     * @param string $socialType
     * @return self
     */
    public function setSocialType($socialType)
    {
        $this->socialType = $socialType;
        return $this;
    }

    /**
     * Get socialType
     *
     * @return string $socialType
     */
    public function getSocialType()
    {
        return $this->socialType;
    }

    /**
     * @return mixed
     */
    public function getDict()
    {
        return $this->dict;
    }

    /**
     * @param mixed $dict
     * @return self
     */
    public function setDict($dict)
    {
        $this->dict = $dict;
        return $this;
    }
}
