<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 29.08.14
 * Time: 10:25
 */

namespace Galmi\Bundle\LexigonBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

/**
 * @MongoDB\Document(repositoryClass="\Galmi\Bundle\LexigonBundle\Document\GameStatRepository")
 */
class GameStat
{
    const GAME_SURVIVE = 'Survive';
    const GAME_EXPLORE = 'Explore';
    const GAME_ESCAPE = 'Escape';

    /**
     * @MongoDB\Id(strategy="auto")
     */
    private $id;

    /**
     * Тип Игры
     *
     * @MongoDB\String
     * @MongoDB\Index(unique=false)
     */
    private $gameType;

    /**
     * Затраченное время на игру, время выхода в игре Escape и т.п.
     *
     * @MongoDB\Int
     * @MongoDB\Index(unique=false, order="asc")
     * @MongoDB\Index(unique=false, order="desc")
     */
    private $time;

    /**
     * Кол-во набранных очков в игре
     *
     * @MongoDB\Int
     * @MongoDB\Index(unique=false, order="desc")
     */
    private $score;

    /**
     * Количество плиток на поле при старте
     *
     * @MongoDB\Int
     */
    private $tilesCount;

    /**
     * Количество удаленных плиток
     *
     * @MongoDB\Int
     */
    private $tilesRemoved;

    /**
     * Количество найденных слов
     *
     * @MongoDB\Int
     */
    private $wordsCount;

    /**
     * @MongoDB\Boolean
     */
    private $win;

    /**
     * @MongoDB\ReferenceOne(targetDocument="\Galmi\Bundle\LexigonBundle\Document\User", inversedBy="gameStats")
     * @MongoDB\Index(unique=false)
     */
    private $user;

    /**
     * Тип соц сети
     *
     * @MongoDB\String
     * @MongoDB\Index(unique=false)
     */
    private $socialType;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gameType
     *
     * @param string $gameType
     * @return self
     */
    public function setGameType($gameType)
    {
        $this->gameType = $gameType;
        return $this;
    }

    /**
     * Get gameType
     *
     * @return string $gameType
     */
    public function getGameType()
    {
        return $this->gameType;
    }

    /**
     * Set time
     *
     * @param int $time
     * @return self
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /**
     * Get time
     *
     * @return int $time
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set score
     *
     * @param int $score
     * @return self
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }

    /**
     * Get score
     *
     * @return int $score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set tilesCount
     *
     * @param int $tilesCount
     * @return self
     */
    public function setTilesCount($tilesCount)
    {
        $this->tilesCount = $tilesCount;
        return $this;
    }

    /**
     * Get tilesCount
     *
     * @return int $tilesCount
     */
    public function getTilesCount()
    {
        return $this->tilesCount;
    }

    /**
     * Set wordsCount
     *
     * @param int $wordsCount
     * @return self
     */
    public function setWordsCount($wordsCount)
    {
        $this->wordsCount = $wordsCount;
        return $this;
    }

    /**
     * Get wordsCount
     *
     * @return int $wordsCount
     */
    public function getWordsCount()
    {
        return $this->wordsCount;
    }

    /**
     * Set tilesRemoved
     *
     * @param int $tilesRemoved
     * @return self
     */
    public function setTilesRemoved($tilesRemoved)
    {
        $this->tilesRemoved = $tilesRemoved;
        return $this;
    }

    /**
     * Get tilesRemoved
     *
     * @return int $tilesRemoved
     */
    public function getTilesRemoved()
    {
        return $this->tilesRemoved;
    }

    /**
     * Set user
     *
     * @param \Galmi\Bundle\LexigonBundle\Document\User $user
     * @return self
     */
    public function setUser(\Galmi\Bundle\LexigonBundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \Galmi\Bundle\LexigonBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set win
     *
     * @param boolean $win
     * @return self
     */
    public function setWin($win)
    {
        $this->win = $win;
        return $this;
    }

    /**
     * Get win
     *
     * @return boolean $win
     */
    public function getWin()
    {
        return $this->win;
    }

    /**
     * Set socialType
     *
     * @param string $socialType
     * @return self
     */
    public function setSocialType($socialType)
    {
        $this->socialType = $socialType;
        return $this;
    }

    /**
     * Get socialType
     *
     * @return string $socialType
     */
    public function getSocialType()
    {
        return $this->socialType;
    }
}
