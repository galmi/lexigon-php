<?php
namespace Galmi\Bundle\LexigonBundle\Document;

use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

/**
 * @MongoDB\Document(repositoryClass="\Galmi\Bundle\LexigonBundle\Document\UserRepository")
 */
class User
{

    const SEX_UNKNOWN = 0;
    const SEX_FEMALE = 1;
    const SEX_MALE = 2;
    /**
     * @MongoDB\Id(strategy="auto")
     */
    private $id;

    /**
     * Тип соц сети
     *
     * @MongoDB\String
     * @MongoDB\Index(unique=false)
     */
    private $socialType;

    /**
     * Идентификатор пользователя в соц сети
     *
     * @MongoDB\String
     * @MongoDB\Index(unique=false)
     */
    private $socialId;

    /**
     * Имя пользователя
     *
     * @MongoDB\String
     */
    private $firstname;

    /**
     * Фамилия пользователя
     *
     * @MongoDB\String
     */
    private $lastname;

    /**
     * Ссылка на домашнюю страницу пользователя
     *
     * @MongoDB\String
     */
    private $url;

    /**
     * Фото профиля пользователя 50х50
     *
     * @MongoDB\String
     */
    private $photo;

    /**
     * Оплачено
     *
     * @MongoDB\Boolean
     */
    private $paid;

    /**
     * Номер заказа
     *
     * @MongoDB\Int
     */
    private $orderId;
    /**
     * Токен пользователя
     *
     * @MongoDB\String
     */
    private $accessToken;

    /**
     * Количество сыгранных игр
     *
     * @MongoDB\Int
     */
    private $gamesPlayed;

    /**
     * Пол игрока
     * 1 — женский;
     * 2 — мужской;
     * 0 — пол не указан.
     *
     * @MongoDB\Int
     */
    private $sex;

    /**
     * Лучший рекорд в Выживании
     *
     * @MongoDB\Int
     * @MongoDB\Index(unique=false, order="desc")
     */
    private $bestSurvive;

    /**
     * Лучший рекорд в Изучении
     *
     * @MongoDB\Int
     * @MongoDB\Index(unique=false, order="desc")
     */
    private $bestExplore;

    /**
     * Лучший рекорд в Спасении
     * @MongoDB\Int
     * @MongoDB\Index(unique=false, order="asc")
     */
    private $bestEscape;

    /**
     * @MongoDB\ReferenceMany(targetDocument="\Galmi\Bundle\LexigonBundle\Document\GameStat", mappedBy="user")
     */
    private $gameStats;

    /**
     * @MongoDB\Collection
     */
    private $achievements = array();

    /**
     * @MongoDB\Int
     */
    private $hints;

    /**
     * @MongoDB\Timestamp
     */
    private $lastUpdate;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set socialType
     *
     * @param string $socialType
     * @return self
     */
    public function setSocialType($socialType)
    {
        $this->socialType = $socialType;
        return $this;
    }

    /**
     * Get socialType
     *
     * @return string $socialType
     */
    public function getSocialType()
    {
        return $this->socialType;
    }

    /**
     * Set socialId
     *
     * @param string $socialId
     * @return self
     */
    public function setSocialId($socialId)
    {
        $this->socialId = $socialId;
        return $this;
    }

    /**
     * Get socialId
     *
     * @return string $socialId
     */
    public function getSocialId()
    {
        return $this->socialId;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string $lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get url
     *
     * @return string $url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return self
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * Get photo
     *
     * @return string $photo
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set paid
     *
     * @param boolean $paid
     * @return self
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
        return $this;
    }

    /**
     * Get paid
     *
     * @return boolean $paid
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     * @return self
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string $accessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     * @return self
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer $orderId
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set gamesPlayed
     *
     * @param integer $gamesPlayed
     * @return self
     */
    public function setGamesPlayed($gamesPlayed)
    {
        $this->gamesPlayed = $gamesPlayed;
        return $this;
    }

    /**
     * Get gamesPlayed
     *
     * @return integer $gamesPlayed
     */
    public function getGamesPlayed()
    {
        return $this->gamesPlayed;
    }

    /**
     * @return integer
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param integer $sex
     * @return self
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @return int
     */
    public function getBestSurvive()
    {
        return $this->bestSurvive;
    }

    /**
     * @param int $bestSurvive
     * @return self
     */
    public function setBestSurvive($bestSurvive)
    {
        $this->bestSurvive = $bestSurvive;
        return $this;
    }

    /**
     * @return int
     */
    public function getBestExplore()
    {
        return $this->bestExplore;
    }

    /**
     * @param int $bestExplore
     * @return self
     */
    public function setBestExplore($bestExplore)
    {
        $this->bestExplore = $bestExplore;
        return $this;
    }

    /**
     * @return int
     */
    public function getBestEscape()
    {
        return $this->bestEscape;
    }

    /**
     * @param int $bestEscape
     * return self
     */
    public function setBestEscape($bestEscape)
    {
        $this->bestEscape = $bestEscape;
        return $this;
    }

    /**
     * @return GameStat[]
     */
    public function getGameStats()
    {
        return $this->gameStats;
    }

    /**
     * Add achievement
     *
     * @param collection $achievement
     * @return self
     */
    public function addAchievement($achievement)
    {
        if (!in_array($achievement, $this->achievements))
            $this->achievements[] = $achievement;
    }

    /**
     * Get achievement
     *
     * @return collection $achievement
     */
    public function getAchievements()
    {
        return $this->achievements;
    }

    /**
     * @return mixed
     */
    public function getHints()
    {
        return $this->hints;
    }

    /**
     * @param int $hints
     * @return $this
     */
    public function setHints($hints)
    {
        $this->hints = $hints;
        return $this;
    }

    public function __construct()
    {
        $this->gameStats = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add gameStat
     *
     * @param \Galmi\Bundle\LexigonBundle\Document\GameStat $gameStat
     */
    public function addGameStat(\Galmi\Bundle\LexigonBundle\Document\GameStat $gameStat)
    {
        $this->gameStats[] = $gameStat;
    }

    /**
     * Remove gameStat
     *
     * @param \Galmi\Bundle\LexigonBundle\Document\GameStat $gameStat
     */
    public function removeGameStat(\Galmi\Bundle\LexigonBundle\Document\GameStat $gameStat)
    {
        $this->gameStats->removeElement($gameStat);
    }

    /**
     * Set achievements
     *
     * @param collection $achievements
     * @return self
     */
    public function setAchievements($achievements)
    {
        $this->achievements = $achievements;
        return $this;
    }

    /**
     * @param $count
     * @return $this
     */
    public function addHints($count)
    {
        $userHints = $this->getHints();
        if (is_null($userHints)) {
            $userHints = 0;
        }
        $userHints += $count;
        $this->setHints($userHints);
        return $this;
    }

    /**
     * @return int|mixed
     */
    public function useHint()
    {
        $hints = $this->getHints();
        if (is_null($hints)) {
            $hints = 0;
        }
        if ($hints > 0) {
            $hints--;
        }
        $this->setHints($hints);
        return $hints;
    }

    /**
     * @return mixed
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * @param mixed $lastUpdate
     * @return $this
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
        return $this;
    }
}
