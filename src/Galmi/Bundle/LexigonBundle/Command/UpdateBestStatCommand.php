<?php

namespace Galmi\Bundle\LexigonBundle\Command;


use Galmi\Bundle\LexigonBundle\Document\GameStat;
use Galmi\Bundle\LexigonBundle\Document\GameStatRepository;
use Galmi\Bundle\LexigonBundle\Document\User;
use Galmi\Bundle\LexigonBundle\Document\UserRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateBestStatCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('galmi:lexigon:updateUserStat')
            ->setDescription('Update user\'s best scores');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        /** @var UserRepository $userRepository */
        $userRepository = $dm->getRepository('GalmiLexigonBundle:User');
        /** @var GameStatRepository $gameStatRepository */
        $gameStatRepository = $dm->getRepository('GalmiLexigonBundle:GameStat');
        /** @var User[] $users */
        $users = $userRepository->findBy(
            array('gamesPlayed' => array('$gt' => 1))
        );
        foreach ($users as $user) {
            $userStats = $user->getGameStats();
            $bestExplore = null;
            $bestSurvive = null;
            $bestEscape = null;
            foreach ($userStats as $gameStat) {
                switch ($gameStat->getGameType()) {
                    case GameStat::GAME_EXPLORE:
                        $score = $gameStat->getScore();
                        if (empty($bestExplore) || $bestExplore < $score) {
                            $bestExplore = $score;
                        }
                        break;
                    case GameStat::GAME_ESCAPE:
                        $score = $gameStat->getTime();
                        if (empty($bestEscape) || $bestEscape > $score) {
                            $bestEscape = $score;
                        }
                        break;
                    case GameStat::GAME_SURVIVE:
                        $score = $gameStat->getTime();
                        if (empty($bestSurvive) || $bestSurvive < $score) {
                            $bestSurvive = $score;
                        }
                }
            }
            if (!empty($bestExplore)) {
                $user->setBestExplore($bestExplore);
            }
            if (!empty($bestSurvive)) {
                $user->setBestSurvive($bestSurvive);
            }
            if (!empty($bestEscape)) {
                $user->setBestEscape($bestEscape);
            }
            $dm->persist($user);
            $dm->flush();
        }
        $output->writeln('ok');
    }


} 