<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 04.12.14
 * Time: 12:49
 */

namespace Galmi\Bundle\LexigonBundle\Command;


use Galmi\Bundle\LexigonBundle\Document\GameStatRepository;
use Galmi\Bundle\LexigonBundle\Document\User;
use Galmi\Bundle\LexigonBundle\Document\UserRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAchieveCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('galmi:lexigon:updateAchieve')
            ->setDescription('Update user\'s achievements');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        /** @var UserRepository $userRepository */
        $userRepository = $dm->getRepository('GalmiLexigonBundle:User');
        /** @var GameStatRepository $gameStatRepository */
        $gameStatRepository = $dm->getRepository('GalmiLexigonBundle:GameStat');
        /** @var User[] $users */
        $users = $userRepository->findBy(
            array(
                'gamesPlayed' => array('$gt' => 0),
                'achievements' => array('$nin' => array("the_graduate"))
            )
        );
        foreach ($users as $user) {
            $user->addAchievement('the_graduate');
            $dm->persist($user);
            $dm->flush();
        }
        $output->writeln('ok');
    }

}