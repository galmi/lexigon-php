<?php
namespace Galmi\Bundle\LexigonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendVkNotificationsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('galmi:lexigon:sendVkNotifications')
            ->setDescription('Send message to all users in app');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $vkService = $this->getContainer()->get('galmi_lexigon.social.vk');
    }
}