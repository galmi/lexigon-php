<?php

namespace Galmi\Bundle\LexigonBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('galmi_lexigon');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->arrayNode('vk')
                    ->children()
                        ->scalarNode('app_id')->isRequired()->end()
                        ->scalarNode('secret')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('fb')
                    ->children()
                        ->scalarNode('app_id')->isRequired()->end()
                        ->scalarNode('secret')->isRequired()->end()
                        ->scalarNode('app_access_token')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('ok')
                    ->children()
                        ->scalarNode('app_id')->isRequired()->end()
                        ->scalarNode('public_key')->isRequired()->end()
                        ->scalarNode('secret')->isRequired()->end()
                        ->scalarNode('offer_key')->isRequired()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
