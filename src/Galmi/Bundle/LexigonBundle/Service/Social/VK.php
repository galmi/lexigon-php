<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 27.08.14
 * Time: 8:51
 */

namespace Galmi\Bundle\LexigonBundle\Service\Social;


use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Galmi\Bundle\LexigonBundle\Document\User;
use Galmi\Bundle\LexigonBundle\Document\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

class VK extends Social
{
    private static $price = 1; //цена приложения в голосах
    const API_URL = 'https://api.vk.com/method/';
    private $accessToken = null;

    public function __construct(ManagerRegistry $dm, $appId, $appSecret)
    {
        $this->dm = $dm;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }

    public function checkRequest(Request $request)
    {
        $authKey = md5(
            $request->get('api_id') . '_' .
            $request->get('viewer_id') . '_' .
            $this->appSecret
        );
        return $authKey == $request->get('auth_key');
    }

    /**
     * @param Request $request
     * @return User
     */
    public function updateUserData(Request $request)
    {
        /** @var UserRepository $repository */
        $repository = $this->dm->getManager()->getRepository('GalmiLexigonBundle:User');
        $hasUser = $repository->getUser(Social::VKONTAKTE, $request->get('viewer_id'));
        if (!$hasUser) {
            $user = new User();
            $user
                ->setFirstname($request->get('first_name'))
                ->setLastname($request->get('last_name'))
                ->setSocialType(self::VKONTAKTE)
                ->setSocialId($request->get('viewer_id'))
                ->setAccessToken($request->get('access_token'))
                ->setPaid(false)
                ->setPhoto($request->get('photo_50'))
                ->setUrl('https://vk.com/' . $request->get('domain'))
                ->setSex($request->get('sex'))
                ->setHints(10);
            $this->dm->getManager()->persist($user);
            $this->dm->getManager()->flush();
            return $user;
        } else {
            $hasUser
                ->setPhoto($request->get('photo_50'))
                ->setFirstname($request->get('first_name'))
                ->setLastname($request->get('last_name'))
                ->setSex($request->get('sex'))
                ->setLastUpdate(time());
            $this->dm->getManager()->persist($hasUser);
            $this->dm->getManager()->flush();
            return $hasUser;
        }
    }

    /**
     * Взаимодействие с платежной системой Вконтакте
     *
     * @param array $input
     * @return mixed
     */
    public function checkPayment()
    {
        $input = $_POST;
        $sig = null;
        //проверка подписи
        if (!empty($input['sig'])) {
            $sig = $input['sig'];
            unset($input['sig']);
        }
        ksort($input);
        $str = '';
        foreach ($input as $k => $v) {
            $str .= $k . '=' . $v;
        }

        if ($sig != md5($str . $this->appSecret)) {
            $response['error'] = array(
                'error_code' => 10,
                'error_msg' => 'Несовпадение вычисленной и переданной подписи запроса.',
                'critical' => true
            );
        } else {
            // Подпись правильная
            switch ($input['notification_type']) {
                case 'get_item':
                case 'get_item_test':
                    // Получение информации о товаре
                    $item = $input['item'];
                    $response = $this->getItem($item);
                    break;

                case 'order_status_change':
                case 'order_status_change_test':
                    // Изменение статуса заказа
                    if ($input['status'] == 'chargeable') {
                        $order_id = intval($input['order_id']);

                        // Код проверки товара, включая его стоимость
                        $app_order_id = $input['receiver_id']; // Получающийся у вас идентификатор заказа.

                        $user = $this->dm->getRepository('GalmiLexigonBundle:User')->getUser(Social::VKONTAKTE, $input['receiver_id']);
                        if ($user) {
                            switch ($input['item']) {
                                case 'unlock1':
                                case 'unlock2':
                                    $user->setPaid(true);
                                    $user->setOrderId($order_id);
                                    break;
                                case 'hints5':
                                    $user->addHints(5);
                                    break;
                            }
                            $dm = $this->dm->getManager();
                            $dm->persist($user);
                            $dm->flush();
                        }

                        $response['response'] = array(
                            'order_id' => $order_id,
                            'app_order_id' => $app_order_id,
                        );
                    } else {
                        $response['error'] = array(
                            'error_code' => 100,
                            'error_msg' => 'Передано непонятно что вместо chargeable.',
                            'critical' => true
                        );
                    }
                    break;
                default:
                    $response['error'] = array(
                        'error_code' => 100,
                        'error_msg' => 'Неизвестный запрос',
                        'critical' => true
                    );
                    break;
            }
        }
        return $response;
    }

    public function sendNotification($uids, $message)
    {
        $params = array();
        $params['v'] = '5.27';
        $params['user_ids'] = implode(',', $uids);
        $params['access_token'] = $this->getAccessToken();
        $params['client_secret'] = $this->appSecret;
        $params['message'] = $message;
        ksort($params);
        $query = self::API_URL . 'secure.sendNotification' . '?' . $this->paramsToUrl($params);
        echo $query . "\n";
        $res = file_get_contents($query);
        $response = json_decode($res, true);
        if (isset($response['error'])) {
            echo $response['error']['error_msg'];
        }
        return $response;
    }

    /**
     * Получение серверного токена
     * @return mixed
     * @throws Exception
     */
    public function getAccessToken()
    {
        // https://oauth.vk.com/access_token?client_id=' + CLIENT_ID + '&client_secret=' + CLIENT_SECRET + '&v=5.27&grant_type=client_credentials
        if ($this->accessToken) {
            return $this->accessToken;
        }
        $params = array(
            'client_id' => $this->appId,
            'client_secret' => $this->appSecret,
            'v' => '5.27',
            'grant_type' => 'client_credentials'
        );
        $query = 'https://oauth.vk.com/access_token?' . $this->paramsToUrl($params);
        $res = file_get_contents($query);
        $decodedRes = json_decode($res, true);
        if (!$decodedRes['access_token']) {
            throw new Exception('Ошибка получения токена');
        }
        $this->accessToken = $decodedRes['access_token'];
        return $decodedRes['access_token'];
    }

    /**
     * Преобразование ассоциативного массива в строку для GET запроса
     * @param $params
     * @return string
     */
    protected function paramsToUrl($params)
    {
        $pice = array();
        foreach ($params as $k => $v) {
            $pice[] = $k . '=' . urlencode($v);
        }
        return implode('&', $pice);
    }

    private function getItem($item)
    {
        $response = [];
        $message = 'Разблокировать все игры и убрать рекламу';
        switch ($item) {
            case 'unlock1':
                $response['response'] = array(
                    'item_id' => 1,
                    'title' => $message,
                    'expiration' => 86400,
                    //'photo_url' => 'http://somesite/images/coin.jpg',
                    'price' => self::$price
                );
                break;
            case'unlock2':
                $response['response'] = array(
                    'item_id' => 2,
                    'title' => $message,
                    'expiration' => 86400,
                    //'photo_url' => 'http://somesite/images/coin.jpg',
                    'price' => self::$price * 2
                );
                break;
            case 'hints5':
                $response['response'] = array(
                    'item_id' => 3,
                    'title' => 'Купить 5 подсказок.',
                    'expiration' => 86400,
                    'price' => self::$price
                );
                break;
            default:
                $response['error'] = array(
                    'error_code' => 100,
                    'error_msg' => 'Неизвестный товар',
                    'critical' => true
                );
        }
        return $response;
    }
}