<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 27.08.14
 * Time: 8:51
 */

namespace Galmi\Bundle\LexigonBundle\Service\Social;


use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Facebook\GraphObject;
use Galmi\Bundle\LexigonBundle\Document\User;
use Galmi\Bundle\LexigonBundle\Document\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;

class FB extends Social
{
    const UNLOCK_PRODUCT = 'https://hex.moisocialki.ru/res/fb/unlock.html';
    const HINTS_PRODUCT = 'https://hex.moisocialki.ru/res/fb/hints.html';

    private $appAccessToken;

    public function __construct(ManagerRegistry $dm, $appId, $appSecret, $appAccessToken)
    {
        $this->dm = $dm;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->appAccessToken = $appAccessToken;
    }

    public function checkRequest(Request $request)
    {
        $signedRequest = $request->get('signed_request');
        $unSignedData = $this->parse_signed_request($signedRequest);
        return $unSignedData;
    }

    /**
     * @param Request $request
     * @return User
     */
    public function updateUserData(Request $request)
    {
        /** @var UserRepository $repository */
        $repository = $this->dm->getManager()->getRepository('GalmiLexigonBundle:User');
        $hasUser = $repository->getUser(Social::FACEBOOK, $request->get('id'));
        $photo50 = 'https://graph.facebook.com/v2.1/' . $request->get('id') . '/picture?redirect=1&type=square';
        if (!$hasUser) {
            $user = new User();
            $user
                ->setFirstname($request->get('first_name'))
                ->setLastname($request->get('last_name'))
                ->setSocialType(self::FACEBOOK)
                ->setSocialId($request->get('id'))
//                ->setAccessToken($request->get('access_token'))
                ->setPaid(false)
                ->setPhoto($request->get($photo50))
                ->setUrl($request->get('link'))
                ->setHints(10);
            $sex = $request->get('gender', User::SEX_UNKNOWN);
            if ($sex === 'female') {
                $user->setSex(User::SEX_FEMALE);
            } else if ($sex === 'male') {
                $user->setSex(User::SEX_MALE);
            } else {
                $user->setSex(User::SEX_UNKNOWN);
            }
            $this->dm->getManager()->persist($user);
            $this->dm->getManager()->flush();
            return $user;
        } else {
            $hasUser
                ->setPhoto($photo50)
                ->setFirstname($request->get('first_name'))
                ->setLastname($request->get('last_name'))
                ->setLastUpdate(time());
            $this->dm->getManager()->persist($hasUser);
            $this->dm->getManager()->flush();
            return $hasUser;
        }
    }

    /**
     * Взаимодействие с платежной системой Фейсбука
     *
     * @param array $input
     * @return mixed
     */
    public function checkPayment()
    {
        $success = false;
        $data = file_get_contents("php://input");
        try {
            $json = json_decode($data);
            if ($json->object == 'payments' && $paymentsId = $json->entry[0]->id) {
                $paymentDetails = $this->getPaymentDetails($paymentsId);
                $paymentDetails = $paymentDetails->asArray();
                if ($paymentDetails && $paymentDetails['application']->id == $this->appId) {
                    if ($paymentDetails['actions'][0]->type == 'charge' && $paymentDetails['actions'][0]->status == 'completed') {
                        $userId = $paymentDetails['user']->id;
                        /** @var User $user */
                        $user = $this->dm->getRepository('GalmiLexigonBundle:User')->getUser(Social::FACEBOOK, $userId);
                        if ($user) {
                            $item = $paymentDetails['items'][0];
                            switch($item->product) {
                                case self::UNLOCK_PRODUCT:
                                    $user->setPaid(true);
                                    $user->setOrderId($paymentsId);
                                    break;
                                case self::HINTS_PRODUCT:
                                    $user->addHints(5);
                                    break;
                            }
                            $dm = $this->dm->getManager();
                            $dm->persist($user);
                            $dm->flush();
                        }
                        $success = true;
                    }
                }
            }
        } catch (\Exception $e) {
            $success = false;
        }
        return $success;
    }

    /**
     * @param $paymentId
     * @return bool|GraphObject
     */
    protected function getPaymentDetails($paymentId)
    {
        FacebookSession::setDefaultApplication(
            $this->appId,
            $this->appSecret
        );
        $session = new FacebookSession($this->appAccessToken);
        try {
            return (new FacebookRequest(
                $session, 'GET', "/{$paymentId}"
            ))->execute()->getGraphObject();
        } catch (FacebookRequestException $e) {
            // The Graph API returned an error
        } catch (\Exception $e) {
            // Some other error occurred
        }
        return false;
    }

    protected function parse_signed_request($signed_request)
    {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2);

        // decode the data
        $sig = $this->base64_url_decode($encoded_sig);
        $data = json_decode($this->base64_url_decode($payload), true);

        // confirm the signature
        $expected_sig = hash_hmac('sha256', $payload, $this->appSecret, $raw = true);
        if ($sig !== $expected_sig) {
            error_log('Bad Signed JSON signature!');
            return null;
        }

        return $data;
    }

    protected function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    public function generate_signed_request($data)
    {
        $payload = $this->base64_url_encode(json_encode($data));

        $expected_sig = hash_hmac('sha256', $payload, $this->appSecret, $raw = true);
        $sig = $this->base64_url_encode($expected_sig);

        return implode('.', [$sig, $payload]);
    }

    protected function base64_url_encode($input)
    {
        return base64_encode($input);
    }


}