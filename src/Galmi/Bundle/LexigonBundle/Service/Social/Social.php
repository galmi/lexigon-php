<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 27.08.14
 * Time: 8:49
 */

namespace Galmi\Bundle\LexigonBundle\Service\Social;


use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

abstract class Social {

    const VKONTAKTE     = 'vk'; //Вконтакте
    const FACEBOOK      = 'fb'; //Фейсбук
    const ODNOKLASSNIKI = 'ok'; //Одноклассники
    const MOI_MIR       = 'mm'; //Мой мир

    protected $container;
    /** @var ManagerRegistry $dm */
    protected $dm;
    protected $appId;
    protected $appSecret;

    /**
     * @param Request $request
     * @return mixed
     */
    abstract public function checkRequest(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    abstract public function updateUserData(Request $request);

    /**
     * @return mixed
     */
    abstract public function checkPayment();
} 