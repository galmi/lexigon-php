<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 03.11.14
 * Time: 17:01
 */

namespace Galmi\Bundle\LexigonBundle\Service\Social;

use Galmi\Bundle\LexigonBundle\Document\User;
use Galmi\Bundle\LexigonBundle\Document\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

class OK extends Social
{
    const ERROR_TYPE_UNKNOWN = 1;
    const ERROR_TYPE_SERVISE = 2;
    const ERROR_TYPE_CALLBACK_INVALID_PYMENT = 3;
    const ERROR_TYPE_SYSTEM = 9999;
    const ERROR_TYPE_PARAM_SIGNATURE = 104;
    // массив пар код продукта => цена
    private $catalog = array(
        "1" => 1,
        "50" => 50,
        "100" => 100,
        "hints5" => 50,
        "hints10" => 80,
        "hints15" => 100
    );
    // массив пар код ошибки => описание
    private static $errors = array(
        1 => "UNKNOWN: please, try again later. If error repeats, contact application support team.",
        2 => "SERVICE: service temporary unavailible. Please try again later",
        3 => "CALLBACK_INVALID_PAYMENT: invalid payment data. Please try again later. If error repeats, contact application support team. ",
        9999 => "SYSTEM: critical system error. Please contact application support team.",
        104 => "PARAM_SIGNATURE: invalid signature. Please contact application support team."
    );

    private $appPublicKey;
    private $offerKey;

    public function __construct(ManagerRegistry $dm, $appId, $appPublicKey, $appSecret, $offerKey)
    {
        $this->dm = $dm;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->appPublicKey = $appPublicKey;
        $this->offerKey = $offerKey;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function checkRequest(Request $request)
    {
        $authKey = md5(
            $request->get('logged_user_id') .
            $request->get('session_key') .
            $this->appSecret
        );
        if ($authKey == $request->get('auth_sig')) {
            return true;
        }
        return false;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateUserData(Request $request)
    {
        /** @var UserRepository $repository */
        $repository = $this->dm->getManager()->getRepository('GalmiLexigonBundle:User');
        $userData = $request->get('userData');
        $hasUser = $repository->getUser(Social::ODNOKLASSNIKI, $userData['uid']);
        if (!$hasUser) {
            $user = new User();
            $user
                ->setFirstname($userData['first_name'])
                ->setLastname($userData['last_name'])
                ->setSocialType(self::ODNOKLASSNIKI)
                ->setSocialId($userData['uid'])
                ->setAccessToken($request->get('session_key'))
                ->setPaid(false)
                ->setPhoto($userData['pic_1'])
                ->setUrl('http://www.ok.ru/profile/' . $userData['uid'])
                ->setHints(10);
            $sex = $request->get('gender', User::SEX_UNKNOWN);
            if ($sex === 'female') {
                $user->setSex(User::SEX_FEMALE);
            } else if ($sex === 'male') {
                $user->setSex(User::SEX_MALE);
            } else {
                $user->setSex(User::SEX_UNKNOWN);
            }
            $this->dm->getManager()->persist($user);
            $this->dm->getManager()->flush();
            return $user;
        } else {
            $hasUser
                ->setPhoto($userData['pic_1'])
                ->setFirstname($userData['first_name'])
                ->setLastname($userData['last_name'])
                ->setLastUpdate(time());
            $this->dm->getManager()->persist($hasUser);
            $this->dm->getManager()->flush();
            return $hasUser;
        }
    }

    /**
     * @return mixed
     */
    public function checkPayment()
    {
        if (array_key_exists("product_code", $_GET) && array_key_exists("amount", $_GET) && array_key_exists("sig", $_GET)) {
            if ($this->checkProduct($_GET["product_code"], $_GET["amount"])) {
                if ($_GET["sig"] == $this->calcSignature($_GET)) {
                    $this->saveTransaction($_GET["product_code"], $_GET['uid'], $_GET['transaction_id']);
                    return $this->returnPaymentOK();
                } else {
                    // здесь можно что-нибудь сделать, если подпись неверная
                    return $this->returnPaymentError(self::ERROR_TYPE_PARAM_SIGNATURE);
                }
            } else {
                // здесь можно что-нибудь сделать, если информация о покупке некорректна
                return $this->returnPaymentError(self::ERROR_TYPE_CALLBACK_INVALID_PYMENT);
            }
        } else {
            // здесь можно что-нибудь сделать, если информация о покупке или подпись отсутствуют в запросе
            return $this->returnPaymentError(self::ERROR_TYPE_CALLBACK_INVALID_PYMENT);
        }
        return null;
    }

    /**
     * Оплата по пратнерской программе http://www.ok.ru/app/traffic
     */
    public function checkOfferPayment()
    {
        $result = ['error' => 0];
        $uid = $_REQUEST['uid'];
        $value = $_REQUEST['value'];
        $sig = $_REQUEST['sig'];

        $checkSig = md5('uid=' . $uid . 'value=' . $value . $this->offerKey);
        if ($checkSig == $sig) {
            $user = $this->dm->getRepository('GalmiLexigonBundle:User')->getUser(Social::ODNOKLASSNIKI, $uid);
            if ($user) {
                $user->addHints($value);
                $dm = $this->dm->getManager();
                $dm->persist($user);
                $dm->flush();
            } else {
                $result = ['error' => 1, 'result' => 'Не найден пользователь'];
            }
        } else {
            $result = ['error' => 1, 'result' => 'Ошибка подписи'];
        }
        return $result;
    }

    // функция провкерки корректности платежа
    private function checkProduct($productCode, $price)
    {
        if (array_key_exists($productCode, $this->catalog) && ($this->catalog[$productCode] == $price)) {
            return true;
        } else {
            return false;
        }
    }

    // функция рассчитывает подпись для пришедшего запроса
    // подробнее про алгоритм расчета подписи можно посмотреть в документации (http://apiok.ru/wiki/pages/viewpage.action?pageId=42476522)
    private function calcSignature($request)
    {
        $tmp = $request;
        unset($tmp["sig"]);
        ksort($tmp);
        $resstr = "";
        foreach ($tmp as $key => $value) {
            $resstr = $resstr . $key . "=" . $value;
        }
        $resstr = $resstr . $this->appSecret;
        return md5($resstr);
    }

    // функция возвращает ответ на сервер одноклассников
    // о корректном платеже
    private function returnPaymentOK()
    {
        $rootElement = 'callbacks_payment_response';

        $dom = $this->createXMLWithRoot($rootElement);
        $root = $dom->getElementsByTagName($rootElement)->item(0);

        // добавление текста "true" в тег <callbacks_payment_response>
        $root->appendChild($dom->createTextNode('true'));

        // генерация xml
        $dom->formatOutput = true;
        $rezString = $dom->saveXML();

        // установка заголовка
//        header('Content-Type: application/xml');
        // вывод xml
        return $rezString;
    }

    // функция возвращает ответ на сервер одноклассников
    // об ошибочном платеже и информацию лб ошибке
    private function returnPaymentError($errorCode)
    {
        $rootElement = 'ns2:error_response';

        $dom = $this->createXMLWithRoot($rootElement);
        $root = $dom->getElementsByTagName($rootElement)->item(0);
        // добавление кода ошибки и описания ошибки
        $el = $dom->createElement('error_code');
        $el->appendChild($dom->createTextNode($errorCode));
        $root->appendChild($el);
        if (array_key_exists($errorCode, self::$errors)) {
            $el = $dom->createElement('error_msg');
            $el->appendChild($dom->createTextNode(self::$errors[$errorCode]));
            $root->appendChild($el);
        }

        // генерация xml
        $dom->formatOutput = true;
        $rezString = $dom->saveXML();

        // добавление необходимых заголовков
//        header('Content-Type: application/xml');
        // ВАЖНО: если не добавить этот заголовок, система может некорректно обработать ответ
        header('invocation-error:' . $errorCode);
        // вывод xml
        return $rezString;
    }

    // Рекомендуется хранить информацию обо всех транзакциях
    public function saveTransaction($productCode, $uid, $orderId)
    {
        $user = $this->dm->getRepository('GalmiLexigonBundle:User')->getUser(Social::ODNOKLASSNIKI, $uid);
        if ($user) {
            switch ($productCode) {
                case "1":
                case "50":
                case "100":
                    $user->setPaid(true);
                    $user->setOrderId($orderId);
                    break;
                case "hints5":
                    $user->addHints(5);
                    break;
                case "hints10":
                    $user->addHints(10);
                    break;
                case "hints15":
                    $user->addHints(15);
                    break;
            }
            $dm = $this->dm->getManager();
            $dm->persist($user);
            $dm->flush();
        }
    }

    // функция создает объект DomDocument и добавляет в него в качестве корневого тега $root
    private function createXMLWithRoot($root)
    {
        // создание xml документа
        $dom = new \DomDocument('1.0');
        // добавление корневого тега
        $root = $dom->appendChild($dom->createElement($root));
        $attr = $dom->createAttribute("xmlns:ns2");
        $attr->value = "http://api.forticom.com/1.0/";
        $root->appendChild($attr);
        return $dom;
    }
}