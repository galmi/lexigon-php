<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 08.11.14
 * Time: 10:29
 */

namespace Galmi\Bundle\LexigonBundle\Service;


use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Galmi\Bundle\LexigonBundle\Document\GameStat;
use Galmi\Bundle\LexigonBundle\Document\User;
use Galmi\Bundle\LexigonBundle\Document\Word;
use Symfony\Component\HttpFoundation\Request;

class AddStat
{
    public function __construct(ManagerRegistry $dm)
    {
        $this->dm = $dm;
    }

    public function add(User $user, Request $request)
    {
        $wordsCount = 0;
        $socialType = $request->get('social');
        $gameType = $request->get('gameType');
        $score = $request->get('score', null);
        $time = $request->get('time', null);
        $dict = $request->get('dict', null);

        $gameStat = new GameStat();
        $gameStat->setGameType($gameType);
        $gameStat->setScore($score);
        $gameStat->setTilesCount($request->get('tilesCount'));
        $gameStat->setTilesRemoved($request->get('tilesRemoved'));
        $gameStat->setTime($time);
        $gameStat->setWin($request->get('win', null));
        $gameStat->setUser($user);
        $gameStat->setSocialType($socialType);

        foreach ($request->get('foundWords', []) as $word => $stat) {
            /** @var Word $wordObject */
            $wordObject = $this->dm->getManager()->getRepository('GalmiLexigonBundle:Word')->findOneBy(['user.id' => $user->getId(), 'word' => $word]);
            if ($wordObject) {
                $count = $wordObject->getCount();
                $wordsCount += $count;
                $wordObject->setCount($count + 1);
                $this->dm->getManager()->persist($wordObject);
//                $this->dm->getManager()->flush();
            } else {
                $wordObject = new Word();
                $wordObject->setUser($user);
                $wordObject->setCount($stat['count']);
                $wordObject->setScore($stat['score']);
                $wordObject->setWord($word);
                $wordObject->setWordLength($stat['length']);
                $wordObject->setSocialType($socialType);
                $wordObject->setDict($dict);
                $this->dm->getManager()->persist($wordObject);
                $wordsCount++;
            }
        }
        $gameStat->setWordsCount($wordsCount);
        $this->dm->getManager()->persist($gameStat);

        $gamesPlayed = $user->getGamesPlayed();
        $user->setGamesPlayed($gamesPlayed + 1);
        $this->dm->getManager()->persist($user);
        $this->checkBestScore($user, $gameType, $score, $time);
        $this->dm->getManager()->flush();
    }

    private function checkBestScore(User $user, $gameType, $score, $time)
    {
        switch ($gameType) {
            case GameStat::GAME_EXPLORE:
                $bestExplore = $user->getBestExplore();
                if (empty($bestExplore) || $bestExplore < $score) {
                    $user->setBestExplore($score);
                    $this->dm->getManager()->persist($user);
                }
                break;
            case GameStat::GAME_ESCAPE:
                $bestEscape = $user->getBestEscape();
                if (empty($bestEscape) || $bestEscape > $time) {
                    $user->setBestEscape($time);
                    $this->dm->getManager()->persist($user);
                }
                break;
            case GameStat::GAME_SURVIVE:
                $bestSurvive = $user->getBestSurvive();
                if (empty($bestSurvive) || $bestSurvive < $time) {
                    $user->setBestSurvive($time);
                    $this->dm->getManager()->persist($user);
                }
        }
    }
} 