<?php

namespace Galmi\Bundle\LexigonBundle\Controller;

use Galmi\Bundle\LexigonBundle\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends Controller
{
    /**
     * Авторизация для приложения в ВК
     * @param Request $request
     * @return JsonResponse
     */
    public function vkAction(Request $request)
    {
        $status = 200;
        if (!$this->get('galmi_lexigon.social.vk')->checkRequest($request)) {
            $responseData = [
                'success' => false,
                'message' => 'Ошибка авторизации'
            ];
            $status = 401;
        } else {
            $user = $this->get('galmi_lexigon.social.vk')->updateUserData($request);
            $responseData = [
                'success' => true,
                'userData' => $this->getUserData($user)
            ];
        }

        $response = new JsonResponse($responseData, $status);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Авторизация для приложения в ФБ
     * @param Request $request
     * @return JsonResponse
     */
    public function fbAction(Request $request)
    {
        $status = 200;
        if (!$this->get('galmi_lexigon.social.fb')->checkRequest($request)) {
            $responseData = [
                'success' => false,
                'message' => 'Ошибка авторизации'
            ];
            $status = 401;
        } else {
            $user = $this->get('galmi_lexigon.social.fb')->updateUserData($request);
            $responseData = [
                'success' => true,
                'userData' => $this->getUserData($user)
            ];
        }

        $response = new JsonResponse($responseData, $status);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Авторизация для приложения в ОК
     * @param Request $request
     * @return JsonResponse
     */
    public function okAction(Request $request)
    {
        $status = 200;
        $responseData = [
            'success' => true
        ];

        if (!$this->get('galmi_lexigon.social.ok')->checkRequest($request)) {
            $responseData = [
                'success' => false,
                'message' => 'Ошибка авторизации'
            ];
            $status = 401;
        } else {
            $user = $this->get('galmi_lexigon.social.ok')->updateUserData($request);
            $responseData = [
                'success' => true,
                'userData' => $this->getUserData($user)
            ];

            //Доп поле для отображения данных офферов в игре
            $responseData['userData']['md5'] = md5('pid=556563134720_1'.'uid='.$user->getSocialId().$this->container->getParameter('galmi_lexigon.ok.offer_key'));
        }

        $response = new JsonResponse($responseData, $status);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function getUserData(User $user)
    {
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        return [
            'id' => $user->getSocialId(),
            'paid' => $user->getPaid(),
            'gamesPlayed' => $user->getGamesPlayed(),
            'hints' => $user->getHints(),
            'sex' => $user->getSex(),
            'achievements' => $user->getAchievements() ? $user->getAchievements() : [],
            'escapeStreak' => $dm->getRepository('GalmiLexigonBundle:GameStat')->getEscapeStreaks($user)
        ];
    }
}
