<?php

namespace Galmi\Bundle\LexigonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PayController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function vkAction()
    {
        $responseData = $this->container->get('galmi_lexigon.social.vk')->checkPayment();

        $response = new JsonResponse($responseData);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @return JsonResponse
     */
    public function fbAction(Request $request)
    {
        $responseData = array(
            'success' => true
        );

        if ($request->get('hub_mode',null)=='subscribe') {
            return $this->render('GalmiLexigonBundle:Pay:fb.html.twig',
                array('hub_challenge' => $request->get('hub_challenge')));
        }

        $responseData['success'] = $this->get('galmi_lexigon.social.fb')->checkPayment();

        $response = new JsonResponse($responseData);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @return JsonResponse
     */
    public function okAction()
    {
        $responseData = $this->container->get('galmi_lexigon.social.ok')->checkPayment();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/xml');
        $response->setContent($responseData);
        return $response;
    }

    /**
     * @return JsonResponse
     */
    public function okofferAction()
    {
        $responseData = $this->container->get('galmi_lexigon.social.ok')->checkOfferPayment();

        $response = new JsonResponse($responseData);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
