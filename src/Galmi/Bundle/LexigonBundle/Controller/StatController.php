<?php

namespace Galmi\Bundle\LexigonBundle\Controller;

use Galmi\Bundle\LexigonBundle\Document\GameStat;
use Galmi\Bundle\LexigonBundle\Document\User;
use Galmi\Bundle\LexigonBundle\Document\Word;
use Galmi\Bundle\LexigonBundle\Service\Social\Social;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StatController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        /**
         * gameType: 'Explore',
         * foundWords: this.game.foundWords,
         * score: this.game.score,
         * win: win,
         * tilesCount: this.game.tilesCount,
         * tilesRemoved: this.game.tilesRemoved,
         * userId: cc.UserData.id
         */
        if (empty($_POST) || empty($_SERVER["HTTP_REFERER"])) {
            echo 'Нельзя обманывать.';
            exit;
        }
        $string = $_POST['gameType'] . $_POST['score'] . $_POST['tilesCount'] . $_POST['tilesRemoved'] . $_POST['userId'] . $_POST['social'] . $_POST['dict'];
        $hashString = md5($string);
        if ($hashString == $_POST['sign']) {
            if (!in_array($request->get('gameType', false), array(GameStat::GAME_ESCAPE, GameStat::GAME_EXPLORE, GameStat::GAME_SURVIVE))) {
                throw new Exception('Не верный тип игры');
            }
            if (!$request->get('userId', false)) {
                throw new Exception('Не указан идентификатор пользователя');
            }
            $socialType = $request->get('social', Social::ODNOKLASSNIKI);
            if (!in_array($socialType,
                array(Social::VKONTAKTE, Social::FACEBOOK, Social::MOI_MIR, Social::ODNOKLASSNIKI))
            ) {
                throw new Exception('Не указан тип соц сети');
            }
            $dm = $this->container->get('doctrine_mongodb')->getManager();
            /** @var User $user */
            $user = $dm->getRepository('GalmiLexigonBundle:User')->getUser($socialType, $request->get('userId'));
            if (!$user) {
                throw new Exception('Не найден пользователь');
            }
            if ($request->get('foundWords', false)) {
                $this->get('galmi_lexigon.stat.add')->add($user, $request);
            }
            $responseData = array('success' => true);
        } else {
            $responseData = array('success' => false);
        }
        $response = new JsonResponse($responseData);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function userAction(Request $request)
    {
        $social = $request->get('social', Social::ODNOKLASSNIKI);
        if (!in_array($social,
            array(Social::VKONTAKTE, Social::FACEBOOK, Social::MOI_MIR, Social::ODNOKLASSNIKI))
        ) {
            throw new Exception('Не указан тип соц сети');
        }
        $userId = $request->get('userId');
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        /** @var User $user */
        $user = $dm->getRepository('GalmiLexigonBundle:User')->getUser($social, $userId);
        $userStat = $dm->getRepository('GalmiLexigonBundle:GameStat')->bestByUser($user->getId());

        $response = new JsonResponse([
            'success' => true,
            'data' => $userStat
        ]);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function friendsAction(Request $request)
    {
        $social = $request->get('social', Social::ODNOKLASSNIKI);
        if (!in_array($social,
            array(Social::VKONTAKTE, Social::FACEBOOK, Social::MOI_MIR, Social::ODNOKLASSNIKI))
        ) {
            throw new Exception('Не указан тип соц сети');
        }
        $userSocials = $request->get('users', []);
        $users = [];
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        foreach ($userSocials as $userSocialId) {
            /** @var User $user */
            $user = $dm->getRepository('GalmiLexigonBundle:User')->getUser($social, $userSocialId);
            if ($user) {
                $users[] = $user->getId();
            }
        }
        $userStat = $dm->getRepository('GalmiLexigonBundle:GameStat')->bestOfFriends($users);

        $response = new JsonResponse([
            'success' => true,
            'data' => $userStat
        ]);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function allAction()
    {
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        $userStat = $dm->getRepository('GalmiLexigonBundle:User')->getBestStats();

        $response = new JsonResponse([
            'success' => true,
            'data' => $userStat
        ]);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function tutorialAction(Request $request)
    {
        if (!$request->get('userId', false)) {
            throw new Exception('Не указан идентификатор пользователя');
        }
        $socialType = $request->get('social', Social::ODNOKLASSNIKI);
        if (!in_array($socialType,
            array(Social::VKONTAKTE, Social::FACEBOOK, Social::MOI_MIR, Social::ODNOKLASSNIKI))
        ) {
            throw new Exception('Не указан тип соц сети');
        }
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        /** @var User $user */
        $user = $dm->getRepository('GalmiLexigonBundle:User')->getUser($socialType, $request->get('userId'));
        if (!$user) {
            throw new Exception('Не найден пользователь');
        }
        if (!$user->getGamesPlayed()) {
            $user->setGamesPlayed(1);
            $dm->persist($user);
            $dm->flush();
        }
        $response = new JsonResponse([
            'success' => true
        ]);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function bestWordsAction(Request $request)
    {
        $dict = $request->get('dict', Word::DICT_RUSSIAN);
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        $words = $dm->getRepository('GalmiLexigonBundle:Word')->getBestWords($dict);
        $response = new JsonResponse([
            'success' => true,
            'data' => $words
        ]);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function addAchievementAction(Request $request)
    {
        if (!$request->get('id', false)) {
            throw new Exception('Wrong parameter');
        }
        if (!$request->get('userId', false)) {
            throw new Exception('Не указан идентификатор пользователя');
        }
        $socialType = $request->get('social', Social::ODNOKLASSNIKI);
        if (!in_array($socialType,
            array(Social::VKONTAKTE, Social::FACEBOOK, Social::MOI_MIR, Social::ODNOKLASSNIKI))
        ) {
            throw new Exception('Не указан тип соц сети');
        }
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        /** @var User $user */
        $user = $dm->getRepository('GalmiLexigonBundle:User')->getUser($socialType, $request->get('userId'));
        if (!$user) {
            throw new Exception('Не найден пользователь');
        }
        $user->addAchievement($request->get('id'));
        $dm->persist($user);
        $dm->flush();

        $response = new JsonResponse([
            'success' => true,
        ]);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function hintAction(Request $request)
    {
        if (!$request->get('userId', false)) {
            throw new Exception('Не указан идентификатор пользователя');
        }
        $socialType = $request->get('social', Social::ODNOKLASSNIKI);
        if (!in_array($socialType,
            array(Social::VKONTAKTE, Social::FACEBOOK, Social::MOI_MIR, Social::ODNOKLASSNIKI))
        ) {
            throw new Exception('Не указан тип соц сети');
        }
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        /** @var User $user */
        $user = $dm->getRepository('GalmiLexigonBundle:User')->getUser($socialType, $request->get('userId'));
        $hints = $user->useHint();
        $dm->persist($user);
        $dm->flush();

        $response = new JsonResponse([
            'success' => true,
            'hints' => $hints
        ]);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
