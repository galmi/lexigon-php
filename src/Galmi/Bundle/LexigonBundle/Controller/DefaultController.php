<?php

namespace Galmi\Bundle\LexigonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function vkAction()
    {
        return $this->render('GalmiLexigonBundle:Default:vk.html.twig');
    }

    public function fbAction(Request $request)
    {
        if (!$this->container->get('galmi_lexigon.social.fb')->checkRequest($request)) {
            throw new \Exception('Ошибка');
        }
        return $this->render('GalmiLexigonBundle:Default:fb.html.twig',
            array(
                'app_id' => $this->container->getParameter('galmi_lexigon.fb.app_id'),
                'signed_request' => $this->container->get('galmi_lexigon.social.fb')->generate_signed_request(['success' => true])
            )
        );
    }

    public function okAction()
    {
        return $this->render('GalmiLexigonBundle:Default:ok.html.twig');
    }
}
