cc.game.onStart = function () {
    cc.view.setDesignResolutionSize(800, 700, cc.ResolutionPolicy.SHOW_ALL);
    //cc.view.resizeWithBrowserSize(false);
    //load resources
    cc.LoaderScene.preload(g_menu, function () {
        $(window).fontSpy({
            fontSpy: 'Intro',
            callback: function (error) {
                cc.director.runScene(MainMenu.scene());
                updateStat();
                cc.eventManager.dispatchCustomEvent(EVENT_GAME_LOADED);
                try {
                    ga('send', 'event', 'client', cc.sys.os + ' ' + cc.sys.browserType + ': ' + cc._renderType);
                } catch (e) {
                }

            }.bind(this)
        });
        cc.eventManager.addCustomListener(EVENT_DICTIONARY_CHANGED, function () {
            loadDictFull(getDictValue('dictFull'));
            updateStat();
        });
        loadDictFull(getDictValue('dictFull'));
    }, this);
};

function updateStat() {
    var callback = function (data) {
        var friends = data || [];
        friends.push(cc.UserData.id);
        $['when'](
            $['post']('/stat/user', {social: SOCIAL, userId: cc.UserData.id}, function (response) {
                if (response.success) {
                    cc.Statistic['user'] = response['data'];
                }
            }),
            $['post']('/stat/friends', {social: SOCIAL, users: friends}, function (response) {
                if (response.success) {
                    cc.Statistic['friends'] = response['data'];
                }
            }),
            $['post']('/stat/all', {}, function (response) {
                if (response.success) {
                    cc.Statistic['globe'] = response['data'];
                }
            }),
            $['post']('/stat/words', {dict: cc.UserData['dict']}, function (response) {
                if (response.success) {
                    cc.Statistic['words'] = response['data'];
                }
            })
        )['then'](function () {
            cc.eventManager.dispatchCustomEvent(EVENT_LOADED_STAT);
        });
    };
    GetFriends(callback);
}

cc.eventManager.addCustomListener(EVENT_UPDATE_STAT, function () {
    updateStat();
});

//cc.eventManager.removeCustomListeners(EVENT_SEND_STAT);
cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
    var stat = data.getUserData();
    stat['userId'] = cc.UserData['id'];
    stat['social'] = SOCIAL;
    stat['dict'] = cc.UserData['dict'];
    stat['sign'] = stat['gameType'] + stat['score'] + stat['tilesCount'] + stat['tilesRemoved'] + stat['userId'] + stat['social'] + stat['dict'];
    stat['sign'] = md5(stat['sign']);
    $['post']('/stat/add', stat, function (response) {
        if (response.success) {
            cc.eventManager.dispatchCustomEvent(EVENT_UPDATE_STAT);
        } else {
            alert('Ошибка сохранения статистики');
        }
    });
});

cc.eventManager.addCustomListener(EVENT_TUTORIAL_END, function () {
    var data = {};
    data['userId'] = cc.UserData['id'];
    data['social'] = SOCIAL;
    if (!cc.UserData.gamesPlayed) {
        cc.UserData.gamesPlayed = 1;
    }
    $['post']('/stat/tutorial', data, function (response) {
        if (response.success) {
            cc.eventManager.dispatchCustomEvent(EVENT_UPDATE_STAT);
        } else {
            alert('Ошибка сохранения статистики');
        }
    });
});

cc.eventManager.addCustomListener(EVENT_SEND_ACHIEVEMENT, function (data) {
    var achieveData = data.getUserData();
    $['post']('/stat/addAchieve', {
        id: achieveData.id,
        userId: cc.UserData['id'],
        social: SOCIAL
    }, function (response) {
        if (!response.success) {
            alert('Ошибка сохранения достижения');
        }
    });
});

cc.eventManager.addCustomListener(EVENT_HINT_FOUNDED, function () {
    var data = {};
    data['userId'] = cc.UserData['id'];
    data['social'] = SOCIAL;
    $['post']('/hint', data, function (response) {
        if (response.success) {
            cc.UserData['hints'] = response['hints'];
            cc.eventManager.dispatchCustomEvent(EVENT_HINTS_UPDATE);
        } else {
            alert('Ошибка обращения к серверу');
        }
    });
});