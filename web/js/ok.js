var APP_URL = 'http://www.ok.ru/game/1107382528';
var CURRENCY = '100 ОК';
var PayEvent = null;
var PayCount = null;
var SOCIAL = 'ok';
HINTS = {
    5: {
        price: 50,
        currency: 'OK'
    },
    10: {
        price: 80,
        currency: 'OK'
    },
    15: {
        price: 100,
        currency: 'OK'
    }
};
/*
 * Инициализация API.
 * Здесь необходимо изменить 2 параметра:
 */
var rParams = FAPI.Util.getRequestParameters();
FAPI.init(rParams["api_server"], rParams["apiconnection"],
    /*
     * Первый параметр:
     * функция, которая будет вызвана после успешной инициализации.
     */
    function () {
        //FAPI.Client.call({"method":"users.hasAppPermission", "ext_perm":"PUBLISH_TO_STREAM"}, function(status, result, data) {
        //    if (!result) {
        //        FAPI.UI.showPermissions("[\"PUBLISH_TO_STREAM\"]");
        //    }
        //});
        FAPI.Client.call({"method": "users.getCurrentUser", "fields": "first_name,last_name,gender,birthday,pic_1"},
            function (method, result, data) {
                var sendData = $.extend({}, QueryString, {userData: result});
                if (result) {
                    $.post('/auth/ok', sendData, function(response){
                        if (response['success'] != true) {
                            alert('Ошибка авторизации');
                        } else {
                            cc.UserData = $.extend(cc.UserData, response['userData']);
                            cc.game.run();

                            if (typeof BORN != 'undefined') {
                                BORN.GetOffer("556563134720_1", response['userData']['id'], response['userData']['md5']);
                            }
                        }
                    });
                }
            }
        );

        cc.eventManager.addCustomListener(EVENT_PAY, function () {
            PayEvent = EVENT_PAY;
            FAPI.UI.showPayment("Полный доступ", "Разблокировать все функции игры.", 100, 100, null, null, "ok", "true");
        });

        cc.eventManager.addCustomListener(EVENT_BUY_HINTS, function (userData) {
            var count = userData.getUserData();
            //cc.eventManager.dispatchCustomEvent(EVENT_GAME_PAUSED);
            PayEvent = EVENT_BUY_HINTS;
            PayCount = count;
            FAPI.UI.showPayment("Подсказки", "Купить "+count+" подсказок", "hints"+count, HINTS[count].price, null, null, "ok", "true");
        });

        cc.eventManager.addCustomListener(EVENT_MAIN_MENU_LOADED, function () {
            //Показываем ссылку с предложением убрать оплату
            var winSize = cc.director.getWinSize();
            if (cc.UserData['paid'] !== true) {
                var removeAdv = new cc.LabelTTF(t('Unlock all for %s', CURRENCY), 'Intro', 14);
                removeAdv.setColor(cc.color(255, 255, 0, 255));
                removeAdv.setPosition(winSize.width - removeAdv.width / 2 - 40, winSize.height - removeAdv.height / 2 - 6 );
                cc.eventManager.addListener({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    onTouchBegan: function (touch, event) {
                        var target = event.getCurrentTarget();
                        var locationInNode = target.convertToNodeSpace(touch.getLocation());
                        var s = target.getContentSize();
                        var rect = cc.rect(0, 0, s.width, s.height);

                        if (cc.rectContainsPoint(rect, locationInNode)) {
                            cc.eventManager.dispatchCustomEvent(EVENT_PAY);
                            return true;
                        }
                        return false;
                    }
                }, removeAdv);
                cc.eventManager.dispatchCustomEvent(EVENT_MAIN_MENU_ADD_CHILD, removeAdv);
            }
        });

        cc.eventManager.addCustomListener(EVENT_MAIN_MENU_LOADED, function () {
            //ДОбавляем кнопку Пригласить друзей
            var winSize = cc.director.getWinSize();
            var inviteFriends = new cc.LabelTTF("Пригласить\nдрузей", 'Intro', 14, null, cc.TEXT_ALIGNMENT_CENTER);
            inviteFriends.setColor(cc.color(255, 255, 0));
            inviteFriends.width = 100;
            inviteFriends.setPosition(inviteFriends.width / 2 + 5, winSize.height - inviteFriends.height / 2 - 5);
            cc.eventManager.addListener({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                onTouchBegan: function (touch, event) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(touch.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {
                        try {
                            ga('send', 'event', 'friends', 'invite');
                        } catch (e) {
                        }
                        FAPI.UI.showInvite("Поиграй в игру Лексигон, проверь свой словарный запас!");
                        return true;
                    }
                    return false;
                }
            }, inviteFriends);
            cc.eventManager.dispatchCustomEvent(EVENT_MAIN_MENU_ADD_CHILD, inviteFriends);
        });
    },
    /*
     * Второй параметр:
     * функция, которая будет вызвана, если инициализация не удалась.
     */
    function (error) {
        alert('Ошибка инициализации');
    }
);

function API_callback(method, result, data)
{
    if (method == "showPayment") {
        if (PayEvent == EVENT_PAY) {
            self.location.reload();
        } else if (PayEvent == EVENT_BUY_HINTS) {
            cc.eventManager.dispatchCustomEvent(EVENT_HINTS_ADDED, PayCount);
        }
    }
}

var GetFriends = function (callback) {
    FAPI.Client.call({"method":"friends.get"}, function(method,result,data) {
        callback(result);
    });
};

//function shareFriends(word) {
//    var message;
//    var found = t('found');
//    console.log(cc.UserData['lang']);
//    if (cc.UserData['lang'] == 'ru') {
//        if (cc.UserData['sex'] == SEX_FEMALE) {
//            found = 'нашла';
//        } else {
//            found = 'нашел';
//        }
//        message = t('I %s word "%s" in game Lexigon. What will you find?', found, word);
//    } else {
//        message = t('I found word "%s" in game Lexigon. What will you find?', word);
//    }
//    FAPI.UI.postMediatopic({
//        media: [
//            {
//                type: "text",
//                text: message
//            },
//            {
//                type: "link",
//                url: APP_URL
//            }
//        ]
//    }, false);
//}
