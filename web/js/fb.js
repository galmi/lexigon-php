var APP_URL = 'https://apps.facebook.com/glexigon/';
var GROUP_URL = 'www.facebook.com/pages/Lexigon-Community/321517924707788';
var GROUP_NAME = 'Lexigon Community';
var SOCIAL = 'fb';

//window['fbAsyncInit'] = function () {
FB['init']({
    appId: '683083608446900',
    cookie: true,
    xfbml: true,
    version: 'v2.1'
});

FB['login'](function (response) {
    // handle the response
    if (!response || response.error) {
        alert('Error occured');
    } else {
        FB['api']('/me', function (response) {
            cc.UserData['id'] = response.id;
            response['signed_request'] = SignedRequest;
            $['post']('/auth/fb', response, function (response) {
                if (response.success != true) {
                    alert('Ошибка авторизации');
                } else {
                    cc.UserData = $.extend({}, cc.UserData, response['userData']);
                    cc.game.run();
                }
            });
        });
    }
}, {scope: 'public_profile,email,user_friends,publish_actions'});

var callback = function (data) {
    if (!data) {
        alert("There was an error processing your payment. Please try again!");
        return;
    }

    if (data['status'] == 'initiated') {
        alert('Платеж будет закончен в течение нескольких минут, пожалуйста обновите страницу с игрой через несколько минут.');
        return;
    }

    if (data['error_code']) {
        if (data['error_code'] != 1383010) {
            alert("There was an error processing your payment." + data['error_message'] + " Error code:" + data['error_code']);
        }
        return;
    }
    self.location.reload();
};

cc.eventManager.addCustomListener(EVENT_PAY, function () {
    FB['ui']({
            method: 'pay',
            action: 'purchaseitem',
            product: 'https://hex.moisocialki.ru/res/fb/unlock.html'
        },
        callback
    );
});

var callbackHints = function (data) {
    cc.eventManager.dispatchCustomEvent(EVENT_GAME_RESUMED);
    if (!data) {
        alert("There was an error processing your payment. Please try again!");
        return;
    }

    if (data['status'] == 'initiated') {
        alert('Payment will finish processing during some minutes. Please update page later.');
        return;
    }

    if (data['error_code']) {
        if (data['error_code'] != 1383010) {
            alert("There was an error processing your payment." + data['error_message'] + " Error code:" + data['error_code']);
        }
        return;
    }
    cc.eventManager.dispatchCustomEvent(EVENT_HINTS_ADDED, 5);
};
cc.eventManager.addCustomListener(EVENT_BUY_HINTS, function () {
    cc.eventManager.dispatchCustomEvent(EVENT_GAME_PAUSED);
    FB['ui']({
            method: 'pay',
            action: 'purchaseitem',
            product: 'https://hex.moisocialki.ru/res/fb/hints.html'
        },
        callbackHints
    );
});

var GetFriends = function (callback) {
    FB['api'](
        "/me/friends",
        function (response) {
            if (response && !response['error']) {
                var friends = [];
                for (var i in response['data']) {
                    friends.push(response['data'][i]['id']);
                }
                callback(friends);
            }
        }
    );
};

function shareFriends(word) {
    var message;
    var found = t('found');
    if (cc.UserData['lang'] == 'ru') {
        if (cc.UserData['sex'] == SEX_FEMALE) {
            found = 'нашла';
        } else {
            found = 'нашел';
        }
        message = t('I %s word "%s"', found, word);
    } else {
        message = t('I found word "%s"', word);
    }
    FB.ui(
        {
            method: "feed",
            display: 'popup',
            message: message,
            caption: message,
            link: APP_URL,
            picture: "https://dl.dropboxusercontent.com/u/3547844/lexigon/logo_256.png"
        },
        function (response) {
            if (response && !response.error) {
                alert(t('Message was published in your stream.'));
            }
        }
    );
}