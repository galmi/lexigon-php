var APP_URL = 'https://vk.com/app4496570';
var GROUP_URL = 'vk.com/lexigon_group';
var GROUP_NAME = 'lexigon_group';
var BannedUsers = ['161041908', '1466311'];
var PayEvent = null;
var SOCIAL = 'vk';

cc.eventManager.addCustomListener(EVENT_MAIN_MENU_LOADED, function () {
    try {
        var winSize = cc.director.getWinSize();
        var arrow = new cc.Sprite('/res/vk/arrow_up.png', cc.rect(0, 0, 24, 24));
        var arrowW = 24;
        var arrowH = 24;

        var rate5stars = new cc.Sprite('/res/vk/rate_5star.png', cc.rect(0, 0, 100, 30));
        var rate5starsW = 100;
        var rate5starsH = 30;

        var arrowX = rate5starsW / 2 + arrowW / 2 - 1;
        var delta = 5;
        var arrowY = winSize.height - arrowH / 2;
        arrow.setPosition(arrowX, arrowY);
        cc.eventManager.dispatchCustomEvent(EVENT_MAIN_MENU_ADD_CHILD, arrow);
        arrow.runAction(cc.sequence(
            cc.repeat(
                cc.sequence(
                    cc.moveTo(.8, cc.p(arrowX, arrowY - delta)),
                    cc.moveTo(.8, cc.p(arrowX, arrowY))
                ), 5
            ),
            cc.callFunc(function () {
                arrow.removeFromParent();
                rate5stars.removeFromParent();

                //ДОбавляем кнопку Пригласить друзей
                var inviteFiriends = new cc.LabelBMFont("Пригласить\nдрузей", res.FntIntro20White, 100, cc.TEXT_ALIGNMENT_CENTER);
                inviteFiriends.setColor(cc.color(255, 255, 0));
                inviteFiriends.width = 100;
                inviteFiriends.setPosition(inviteFiriends.width / 2 + 5, winSize.height - inviteFiriends.height / 2 - 5);
                cc.eventManager.addListener({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    onTouchBegan: function (touch, event) {
                        var target = event.getCurrentTarget();
                        var locationInNode = target.convertToNodeSpace(touch.getLocation());
                        var s = target.getContentSize();
                        var rect = cc.rect(0, 0, s.width, s.height);

                        if (cc.rectContainsPoint(rect, locationInNode)) {
                            try {
                                ga('send', 'event', 'friends', 'invite');
                            } catch (e) {
                            }
                            VK['callMethod']("showInviteBox");
                            return true;
                        }
                        return false;
                    }
                }, inviteFiriends);
                cc.eventManager.dispatchCustomEvent(EVENT_MAIN_MENU_ADD_CHILD, inviteFiriends);
            }, this)
        ));

        rate5stars.setPosition(10 + rate5starsW / 2, winSize.height - rate5starsH / 2 - arrowH - delta);
        cc.eventManager.dispatchCustomEvent(EVENT_MAIN_MENU_ADD_CHILD, rate5stars);

        //Показываем ссылку с предложением убрать оплату
        if (cc.UserData['paid'] !== true) {
            var removeAdv = new cc.LabelTTF(t('Remove AD'), 'Intro', 14);
            removeAdv.setPosition(winSize.width - removeAdv.width / 2 - 50, winSize.height - removeAdv.height / 2 - 6);
            cc.eventManager.addListener({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                onTouchBegan: function (touch, event) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(touch.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {
                        cc.eventManager.dispatchCustomEvent(EVENT_PAY);
                        return true;
                    }
                    return false;
                }
            }, removeAdv);
            cc.eventManager.dispatchCustomEvent(EVENT_MAIN_MENU_ADD_CHILD, removeAdv);
        }
    } catch (e) {
        console.log('[ERROR]', e);
    }
});

VK['init'](function () {
    VK['api']("users.get", {fields: "sex,bdate,photo_50,domain"}, function (data) {
        var userData = data['response'][0];
        if ($.inArray(userData['id'].toString(), BannedUsers) != -1) {
            alert('ЗАБАНЕН!!!');
            return;
        }
        var sendData = $.extend({}, QueryString, userData);
        $['post']('/auth/vk', sendData, function (response) {
            if (response['success'] != true) {
                alert('Ошибка авторизации');
            } else {
                cc.UserData = $.extend(cc.UserData, userData, response['userData']);
                var dateParse = userData['bdate'].split('.');
                if (dateParse[2]) {
                    cc.UserData['age'] = new Date().getFullYear() - dateParse[2];
                }
                cc.game.run();

                try {
                    var user_id = userData['id'];
                    var app_id = 4496570;
                    var a = new VKAdman();
                    a.setupPreroll(app_id);
                    admanStat(app_id, user_id);
                } catch (e) {
                }
            }
        });
    });

    cc.eventManager.addCustomListener(EVENT_GAME_END, function () {
        if (cc.UserData['paid'] !== true) {
            try {
                var currentGame = cc.director.getRunningScene();
                if (!(currentGame instanceof SceneTutorial)) {
                    if (typeof showAdFotostrana !== undefined) {
                        showAdFotostrana();
                    }
                }
            } catch (e) {
                console.log('[ERROR]', e);
            }
        }
    });

    cc.eventManager.addCustomListener(EVENT_PAY, function () {
        PayEvent = EVENT_PAY;
        VK['callMethod']("showOrderBox", {type: 'item', item: 'unlock2'});
    });

    cc.eventManager.addCustomListener(EVENT_BUY_HINTS, function () {
        PayEvent = EVENT_BUY_HINTS;
        cc.eventManager.dispatchCustomEvent(EVENT_GAME_PAUSED);
        VK['callMethod']("showOrderBox", {type: 'item', item: 'hints5'});
    });

    VK['addCallback']('onOrderSuccess', function (order_id) {
        try {
            ga('send', 'event', 'pay', PayEvent);
        } catch (e) {
        }
        if (PayEvent == EVENT_PAY) {
            self.location.reload();
        } else if (PayEvent == EVENT_BUY_HINTS) {
            cc.eventManager.dispatchCustomEvent(EVENT_HINTS_ADDED, 5);
            cc.eventManager.dispatchCustomEvent(EVENT_GAME_RESUMED);
        }

    });
}, function () {
    alert('Ошибка инициализации');
}, '5.24');

var GetFriends = function (callback) {
    VK['api']("friends.getAppUsers", {}, function (data) {
        callback(data['response']);
    });
};

function shareFriends(word) {
    var message;
    var found = t('found');
    if (cc.UserData['lang'] == 'ru') {
        if (cc.UserData['sex'] == SEX_FEMALE) {
            found = 'нашла';
        } else {
            found = 'нашел';
        }
        message = t('I %s word "%s" in game Lexigon. What will you find?', found, word);
    } else {
        message = t('I found word "%s" in game Lexigon. What will you find?', word);
    }
    VK['api']("wall.post", {
        owner: cc.UserData.id,
        message: message + ' ' + APP_URL,
        attachment: 'photo33058161_345774693'
    }, function (data) {
        if (!data['error']) {
            try {
                ga('send', 'event', 'wall', 'post');
            } catch (e) {
            }
            //    alert('Ошибка размещения сообщения');
        }
    });
}